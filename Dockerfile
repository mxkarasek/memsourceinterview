FROM openjdk:8 AS builder
WORKDIR /app
COPY . /app
RUN curl -Ls https://git.io/sbt > /bin/sbt && chmod 0755 /bin/sbt
RUN /bin/sbt assembly

FROM openjdk:8 AS runner
RUN mkdir -p /opt/service
WORKDIR /opt/service
COPY --from=builder /app/target/artifacts/service.jar /opt/service
CMD ["java", "-jar",  "service.jar"]

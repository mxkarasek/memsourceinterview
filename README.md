#Simple Kafka Stream app that counts confirmed segments
## Building
You can use your favorite IDE to import sbt project and build it or you can use provided dockerfile to buld the image:
```aidl
docker build -t kafka-stream-app .
```

##Running
The app requires kafka (and zookeeper) to be running you can run them using `docker compose`
```aidl
docker-compose -f docker-compose-stack.yaml up 
```
To run the app in the IDE you need to change the value of `bootstrapServer` in `KafkaApp.Config` to point to `localhost:9092`.

You can also run the whole stack together with the app using `docker compose`
```aidl
docker-compose -f docker-compose.yaml up
```
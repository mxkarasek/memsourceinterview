
name := "memsource-interview"

version := "0.1"

scalaVersion := "2.13.6"

val stream4sVersion = "0.15.0"

libraryDependencies ++= Seq(
  "com.compstak" %% "kafka-streams4s-core" % stream4sVersion,
  "org.apache.kafka" %% "kafka-streams-scala" % "2.7.0",
  "ch.qos.logback"   % "logback-classic" % "1.2.3",
  "org.scalatest" %% "scalatest" % "3.2.0" % Test,
  "org.apache.kafka" % "kafka-streams-test-utils" % "2.7.0" % Test
)

val circeVersion = "0.14.1"
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

target in assembly := file("target/artifacts")
assemblyJarName := "service.jar"

assemblyMergeStrategy in assembly := {
  case PathList("jackson-annotations-2.10.4.jar", xs @ _*)            => MergeStrategy.last
  case PathList("jackson-core-2.10.4.jar", xs @ _*)                   => MergeStrategy.last
  case PathList("jackson-databind-2.10.4.jar", xs @ _*)               => MergeStrategy.last
  case PathList("jackson-dataformat-cbor-2.10.4.jar", xs @ _*)        => MergeStrategy.last
  case PathList("jackson-datatype-jdk8-2.10.4.jar", xs @ _*)          => MergeStrategy.last
  case PathList("jackson-datatype-jsr310-2.10.4.jar", xs @ _*)        => MergeStrategy.last
  case PathList("jackson-module-parameter-names-2.10.4.jar", xs @ _*) => MergeStrategy.last
  case PathList("jackson-module-paranamer-2.10.4.jar", xs @ _*)       => MergeStrategy.last
  case PathList("reference.conf")                                     => MergeStrategy.concat
  case PathList("META-INF", "MANIFEST.MF")                            => MergeStrategy.discard
  case _                                                              => MergeStrategy.first
}
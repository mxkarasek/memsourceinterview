package memsource.homework

import cats.effect._
import memsource.homework
import org.apache.kafka.streams.{TestInputTopic, TestOutputTopic, TopologyTestDriver}
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.KeyValue
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.apache.kafka.streams.scala.serialization.Serdes

import java.util.Properties
import scala.io.{BufferedSource, Source}
import scala.jdk.CollectionConverters._

class AppTest extends AnyFlatSpec with Matchers {

  "App" should "pass the smoke test" in {
    val expected = Set(new KeyValue("jNazVPlL11HFhTGs1_dc1", "0"), new KeyValue("TyazVPlL11HYaTGs1_dc1", "2"))
    val actual = executeStreamWithFile("/kafka-messages.jsonline").unsafeRunSync().toList

    Set(actual.filter(x => x.key == "jNazVPlL11HFhTGs1_dc1").last,
      actual.filter(x => x.key == "TyazVPlL11HYaTGs1_dc1").last) should be (expected)
  }

  /**
   * Read events from the file and process them and return set of resulted key:values
   */
  def executeStreamWithFile(fileName: String): IO[Iterable[KeyValue[String, String]]] =
    for {
      topics <- createTopics
      _ <- readInput(fileName).use(f => IO(f.getLines().foreach(l => topics._1.pipeInput(l))))
      result <- IO(topics._2.readKeyValuesToList().asScala)
    } yield result

  def readInput(fileName: String): Resource[IO, BufferedSource] =
    Resource.make(IO(Source.fromInputStream(getClass.getResourceAsStream(fileName))))(r => IO(r.close()))

  def createTopics: IO[(TestInputTopic[String, String], TestOutputTopic[String, String])] =
    IO {
      val builder = new StreamsBuilder()
      App.topology(builder, config).unsafeRunSync()
      val testDriver = new TopologyTestDriver(builder.build(), configProps)
      (
        testDriver.createInputTopic(config.sourceTopic, Serdes.stringSerde.serializer(), Serdes.stringSerde.serializer()),
        testDriver.createOutputTopic(config.destTopic, Serdes.stringSerde.deserializer(), Serdes.stringSerde.deserializer()))
    }

  val config: homework.App.Config = App.config
  val configProps: Properties = App.props
}

package memsource.homework

import cats.effect.IO
import io.circe.generic.auto._
import io.circe.jawn.decode
import io.circe.{DecodingFailure, Error}
import memsource.homework.Event._
import memsource.homework.Serdes._
import org.apache.kafka.streams.scala.serialization.Serdes._
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala._

/**
 * Defines stream topology of the application as well as the business logic
 */
object App extends KafkaApp {

  /**
   * Defines topology of the stream.
   * Deliberately ignoring incorrect messages. It would be nice to route them to error topic and replay them later.
   */
  override def topology(builder: StreamsBuilder, config: Config): IO[Unit] =
    IO {
        builder
          .stream[String, Either[Error, ExternalEvent]](config.sourceTopic)
          .map((k, v) => (k, v.flatMap(toInternalEvent)))
          .flatMap((_, v) => extractKey(v))
          .groupByKey
          .aggregate(emptySetInitializer)(confirmedSegmentsAggregator)
          .mapValues(_.fold(_ => 0, s => s.size))
          .toStream
          .mapValues(_.toString)
          .to(config.destTopic)
    }


  /**
   * Aggregates confirmed segments in the Set of segments
   */
  val emptySetInitializer: Either[Error, Set[Int]] = Right(Set.empty[Int])
  def confirmedSegmentsAggregator(key: String, value: Either[Error, Event], acc: Either[Error, Set[Int]]): Either[Error, Set[Int]] =
    value.fold(_ => acc, e => isConfirmedSegment(e) match {
      case Some((segmentId, isConfirmed)) =>
        if (isConfirmed)
          acc.map(segments => segments + segmentId)
        else
          acc.map(segments => segments - segmentId)
      case _ => acc
    })

  /**
   * Transforms External Event to Internal Event
   */
  def toInternalEvent(externalEvent: ExternalEvent): Either[Error, Event] =
    externalEvent match {
      case ExternalEvent(op, Some(after), _) if op == "c" => decode[Payload](after).map(p => AfterEvent(p))
      case ExternalEvent(op, _, Some(patch)) if op == "u" => decode[Payload](patch).map(p => PatchEvent(p))
      case _ => Left(DecodingFailure("Unknown event", List.empty))
    }

  /**
   * Extracts key from Internal event
   */
  def extractKey(errorOrEvent: Either[Error, Event]): Iterable[(String, Either[Error, Event])] =
    errorOrEvent match {
      case Right(event) => event match {
        case AfterEvent(payload) => Some((payload.taskId, Right(event)))
        case PatchEvent(payload) => Some((payload.taskId, Right(event)))
        case _ => None
      }
      case Left(_) => None
    }

  /**
   * Get segment/group id and confirmation value of the segment
   */
  def isConfirmedSegment(event: Event): Option[(Int, Boolean)] = event match {
    case AfterEvent(payload) if payload.levels.nonEmpty && payload.levels.headOption == payload.tUnits.headOption.map(_.confirmedLevel) =>
      Some((payload.tGroupId, true))
    case AfterEvent(payload) =>
      Some((payload.tGroupId, false))
    case _ => None
  }
}

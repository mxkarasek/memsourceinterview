package memsource.homework

import cats.effect.{ExitCode, IO, IOApp}
import compstak.kafkastreams4s.Platform
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.scala.StreamsBuilder

import java.time.Duration
import java.util.Properties


/**
 * Main Loop Kafka Streaming app logic and additional utilities.
 * This is not production grade code.
 */
trait KafkaApp extends IOApp {

  def topology(builder: StreamsBuilder, config: Config): IO[Unit]

  override def run(args: List[String]): IO[ExitCode] = {

    val shutdownStatus = for {
      _ <- topology(builder, config)
      builtTopology <- IO(builder.build)
      status <- Platform.run[IO](builtTopology, props, Duration.ofSeconds(5))
    } yield status

    shutdownStatus.redeemWith(_ => IO.pure(ExitCode.Error), _ => IO.pure(ExitCode.Success))
  }

  class Config(
    //val bootstrapServer: String = "localhost:9092", // when using in IDE
    val bootstrapServer: String = "broker:29092", //when using docker
    val sourceTopic: String = "events",
    val destTopic: String = "task-count")

  val config = new Config

  val props = new Properties
  props.put(StreamsConfig.APPLICATION_ID_CONFIG, "memsource-homework")
  props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
  props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
  props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
  props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, config.bootstrapServer)


  val builder = new StreamsBuilder
}

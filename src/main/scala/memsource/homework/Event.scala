package memsource.homework

/**
 * Domain event model
 */
object Event {

  case class ExternalEvent(op: String, after: Option[String], patch: Option[String])

  sealed trait Event
  case class AfterEvent(after: Payload) extends Event
  case class PatchEvent(patch: Payload) extends Event

  case class tUnit(tUnitId: String, confirmedLevel: Int)
  case class Payload(taskId: String, tGroupId: Int, levels: List[Int], tUnits: List[tUnit])
}

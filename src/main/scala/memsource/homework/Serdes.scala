package memsource.homework

import io.circe.generic.auto._
import io.circe.jawn.decode
import io.circe.syntax._
import io.circe.{Decoder, Encoder, Error}
import memsource.homework.Event.{Event, ExternalEvent}
import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}
import org.apache.kafka.streams.scala.kstream.{Consumed, Grouped, Produced}
import org.apache.kafka.streams.scala.serialization.Serdes.stringSerde


object Serdes {

  class EitherErrorSerde[T : Encoder : Decoder] extends Serde[Either[Error, T]] {
    override def serializer: Serializer[Either[Error, T]] =
      (topic: String, data: Either[Error, T]) => data.fold(e => Array.empty[Byte], d => d.asJson.toString.getBytes)

    override def deserializer: Deserializer[Either[Error, T]] =
      (topic: String, data: Array[Byte]) => decode[T](new String(data))
  }

  object EitherErrorSerde {
    def apply[T : Encoder : Decoder]() = new EitherErrorSerde[T]()
  }

  implicit val eitherErrorSerdeExternalEvent: EitherErrorSerde[ExternalEvent] = EitherErrorSerde[ExternalEvent]()
  implicit val eitherErrorSerdeEvent: EitherErrorSerde[Event] = EitherErrorSerde[Event]()
  implicit val eitherErrorSerdeSetInt: Serde[Either[Error, Set[Int]]] = EitherErrorSerde[Set[Int]]()

  implicit val consumedStringEitherErrorExternalEvent: Consumed[String, Either[Error, ExternalEvent]] =
    Consumed.`with`[String, Either[Error, ExternalEvent]]

  implicit val producedStringEvent: Produced[String, Either[Error, Event]] =
    Produced.`with`[String, Either[Error, Event]]

  implicit val groupedStringEitherErrorEvent: Grouped[String, Either[Error, Event]] =
    Grouped.`with`[String, Either[Error, Event]]
}
